<?php
class apiActions extends sfDoctrineRestBasicActions
{
    /**
     * 
     * default route - if any other was not matched
     * 
     * @param sfWebRequest $request
     * @throws sfException
     */
    public function executeDefault(sfWebRequest $request)
    {
        throw new sfException('Rout does not exisst', 401);
    }
    
    /**
     * get history data
     *
     * @param sfWebRequest $request
     * @return array
     */
    public function executeGetHistory(sfWebRequest $request)
    {
        $result = array();
        
        $data = Doctrine_Core::getTable('RequestHistory')->findAll();
        $result['data'] = $data ? $data->toArray() : null;

        return $result;
    }

    /**
     * get geodata by address
     * 
     * @param sfWebRequest $request
     * @return array
     * @throws sfException
     */
    public function executeGetGeoCodeByAddress(sfWebRequest $request)
    {
        $address = $request->getParameter('address');
        
        //return data from database if address was already requested
        if ($data = Doctrine_Core::getTable('RequestHistory')->getExistGeoDataByAddress($address)) {
            $result = array('data' => array(
                'geo_lat' => $data->getGeoLat(),
                'geo_lng' => $data->getGeoLng()
            ));
        } else {
            $googleApiUrl = sfConfig::get('app_google_api_url');
            $googleApiKey = sfConfig::get('app_google_api_key');
            
            $maps = new GoogleMaps();
            $response = $maps->getGeoCode($googleApiUrl, $address, $googleApiKey);
            
            if (GoogleMaps::STATUS_OK == $response['status']) {
                
                $history = new RequestHistory();
                $history->setAddress($address)
                    ->setUserId($this->getUser()->getAttribute('userId'))
                    ->setStatus($response['status'])
                    ->setGeoLat($response['data']['lat'])
                    ->setGeoLng($response['data']['lng'])
                    ->setRequest($response['request'])
                    ->setResponse(serialize(json_decode($response['response'])))
                    ->save();

                $result = array('data' => array(
                    'geo_lat' => $response['data']['lat'],
                    'geo_lng' => $response['data']['lng']
                ));
            } else {
                $history = new RequestHistory();
                $history->setUserId($this->getUser()->getAttribute('userId'))
                    ->setStatus($response['error'])
                    ->setRequest($response['request'])
                    ->setResponse(serialize(json_decode($response['response'])))
                    ->save();
                throw new sfException($response['error'], 400);
            }
        }
        
        return $result;
    }
    
    /**
     * 
     * delete history record by id
     * 
     * @param sfWebRequest $request
     * @return string
     * @throws sfException
     */
    public function executeDeleteHistoryById(sfWebRequest $request)
    {
        $result = array();
        
        if ( !$data = Doctrine_Core::getTable('RequestHistory')->find($request->getParameter('id')) ) {
            throw new sfException('id_not_found', 400);
        }
        $data->delete();
        $result['data'] = 'success';

        return $result;
    }

    /**
     * 
     * get history record by id
     * 
     * @param sfWebRequest $request
     * @return type
     * @throws sfException
     */
    public function executeGetHistoryById(sfWebRequest $request)
    {
        $result = array();
        if ( !$data = Doctrine_Core::getTable('RequestHistory')->find($request->getParameter('id')) ) {
            throw new sfException('id_not_found', 400);
        }
        $result['data'] = $data->toArray();

        return $result;
    }
}

