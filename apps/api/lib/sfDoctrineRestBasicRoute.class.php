<?php
/**
 * Description of sfDoctrineRestBasicRoute
 *
 */
class sfDoctrineRestBasicRoute extends sfRequestRoute  {

    public function matchesUrl($url, $context = array())
    {
        $parameters = parent::matchesUrl($url, $context);

        if ($parameters)
        {
            //set the action to the requested method
            $parameters['original_action'] = $parameters['action'];
            $parameters['action'] = 'BasicRequest';

            return $parameters;
        }

        return FALSE;
    }

   
}

