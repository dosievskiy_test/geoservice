<?php

/**
 * base class for all of the rest actions
 *
 */
class sfDoctrineRestBasic extends sfActions
{
    /**
     * create response based on exception information
     * 
     * @param Exception obj $e
     */
    public function handleException($e)
    {
        $code = (!$e->getCode() || $e->getCode() > 500) ? 500 : $e->getCode();

        $this->getResponse()->setHttpHeader('Message', $e->getMessage());
        $this->getResponse()->setStatusCode($code);
        $error = array('error' => $e->getMessage());
        
        $this->renderText(json_encode($error));
    }

    /**
     * handles the output to be sent to the rest client
     *
     * @param string $text
     * @param array $additionalHeaders
     * @param int $status
     */
    public function outputRequest($text = null, $additionalHeaders = array(), $status = 200)
    {
        foreach($additionalHeaders as $key=>$val)
        {
            $this->getResponse()->setHttpHeader($key, $val);
        }

        $this->getResponse()->setStatusCode($status);
        if ($this->getRequest()->getMethod() != 'HEAD')
        {
            $this->renderText($text);
        }
    }

    /**
     * serialize data
     *
     * @param array $data
     * @param string $type
     * @throws sfException 
     * @return json
     */
    public function serializeData($data, $type = 'json')
    {
        if (!$result = json_encode($data)) {
            throw new sfException('Error occured', 500);
        }
       
        return $result;
    }

    /**
     * 
     * check token
     * 
     * @throws sfException
     */
    protected function checkToken()
    {
        $token = $this->getRequestParameter('token');
        if (!$token) {
            throw new sfException('Token is required', 401);
        }
        
        if (!$user = Doctrine_Core::getTable('User')->getUserByToken($token)) {
            throw new sfException('Token is invalid', 401);
        } else {
            $this->getUser()->setAttribute('userId', $user->getId());
        }
    }

}

