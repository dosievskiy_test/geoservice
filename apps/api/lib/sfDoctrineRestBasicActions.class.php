<?php

/**
 * class handles the basic rest actions
 *
 */
class sfDoctrineRestBasicActions extends sfDoctrineRestBasic
{
    /**
     * do all the checks before a result is produced
     */
    protected function checkRequest()
    {
        $this->checkToken();
    }    

    /**
     * basic request
     *
     * @param sfWebRequest $request
     * @return null
     */
    public function executeBasicRequest(sfWebRequest $request)
    {
        try
        {
            $this->checkRequest();
            
            $module = $this->getRequest()->getParameter('module');
            $action = $this->getRequest()->getParameter('original_action');
            $context = $this->getContext();
                    
            $fullModuleName = $module . 'Actions'; //e.g. ApiActions
            $this->apiModule = new $fullModuleName($context, 'api', $action);
            $fullActionName = 'execute' . $action; //e.g. executeIndex
            $data = $this->apiModule->$fullActionName($request);
            $data = $this->serializeData($data);
            
            $this->outputRequest($data);
        } catch (Exception $e) {
            $this->handleException($e);
        }

        return sfView::NONE;
    }

}
