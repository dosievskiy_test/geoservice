<?php

/*
 * class for get geodata by address via google maps api.
 */
class GoogleMaps
{
    const STATUS_OK = 'OK';
    const STATUS_ZERO_RESULTS = 'ZERO_RESULTS';
    const STATUS_OVER_QUERY_LIMIT = 'OVER_QUERY_LIMIT';
    const STATUS_REQUEST_DENIED ='REQUEST_DENIED';
    const STATUS_INVALID_REQUEST = 'INVALID_REQUEST';
    const STATUS_UNKNOWN_ERROR = 'UNKNOWN_ERROR';
    
    const STATUS_FAILED = 'failed'; 
    
    public function __construct() {
    }
    
    /*
     * get geodata by address
     * 
     * @param string $url
     * @param string $address
     * @param string $key
     * @return array
     */
    public function getGeoCode($url, $address, $key)
    {
        $url = $url . '?address=' . urlencode($address) . '?key=' . $key;

        $response = file_get_contents($url);
        $result = array(
            'status' => self::STATUS_OK,
            'request' => $url,
            'response' => $response
        );
        $response = json_decode($response);
        
        if ($response->status == self::STATUS_OK) {
            if ($response->results[0]->geometry->location->lat && $response->results[0]->geometry->location->lng) {
                $result['data'] = array(
                    'lat' => $response->results[0]->geometry->location->lat,
                    'lng' => $response->results[0]->geometry->location->lng
                );
            } else {
                $result['status'] = self::STATUS_FAILED;
                $result['error'] = 'empty data';
            }
        } else {
            $result['status'] = self::STATUS_FAILED;
            $result['error'] = $response->status;
            $result['message'] = $response->error_message ? $response->error_message : '';
        }
        
        return $result;
    }
}