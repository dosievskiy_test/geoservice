<?php

/**
 * RequestHistory form base class.
 *
 * @method RequestHistory getObject() Returns the current form's model object
 *
 * @package    geoservice
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseRequestHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'address'    => new sfWidgetFormInputText(),
      'status'     => new sfWidgetFormInputText(),
      'geo_lat'    => new sfWidgetFormInputText(),
      'geo_lng'    => new sfWidgetFormInputText(),
      'request'    => new sfWidgetFormTextarea(),
      'response'   => new sfWidgetFormTextarea(),
      'user_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => false)),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'address'    => new sfValidatorString(array('max_length' => 255)),
      'status'     => new sfValidatorString(array('max_length' => 255)),
      'geo_lat'    => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'geo_lng'    => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'request'    => new sfValidatorString(array('max_length' => 400, 'required' => false)),
      'response'   => new sfValidatorString(array('required' => false)),
      'user_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('User'))),
      'created_at' => new sfValidatorDateTime(),
      'updated_at' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('request_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'RequestHistory';
  }

}
