<?php

/**
 * UserTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class UserTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object UserTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('User');
    }
    
    /**
     * 
     * get user by token
     * 
     * @param string $token
     * @return User
     */
    public function getUserByToken($token)
    {
        $q = $this->createQuery('u')
            ->leftJoin('u.UserToken t')
            ->where('t.token = ?', $token)
            ->limit(1);
        
        return $q->fetchOne(array());
    }
}